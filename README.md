### Philosophy
---
- Keep it simple.
- Be efficient, and don't kill every PC you meet.

### Notes
---
- Shifts the pause menu down towards the bottom of the screen.
- The "Achievements" and "Invite a Friend" is removed.
- Using a keyboard/controller and selecting "Call A Vote" will instantly highlight "Kick Players".

### Installation
---
- Official Releases
	- Subscribe to the mod on the [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2676177996) *or* download the .VPK file [from here on GitLab](https://gitlab.com/unryker/left-4-dead-2-addons/rykers-pause-menu/-/releases) and put the VPK in your `steamapps/common/Left 4 Dead 2/left4dead2/addons` folder.
- Bleeding-edge Releases
	- Download the [source code](https://gitlab.com/unryker/left-4-dead-2-addons/rykers-pause-menu) as a .ZIP here. Extract the folder inside the .ZIP to `left4dead2/addons`. Drag and drop the extracted folder onto VPK.exe inside `left 4 dead 2/bin/`. Install the newly-made .VPK file into `left 4 dead 2/left4dead2/addons`.

### Compatibility
---
- Compatible with anything as long as it doesn't directly modify the pause menu, such as Urik HUD, Simple HUD, and NeunGUI.
	- Note I said Urik **HUD**, not Urik **Menu**.
- Compatible with [Ryker's Main Menu.](https://gitlab.com/unryker/left-4-dead-2-addons/rykers-main-menu)