"Resource/UI/InGameMainMenu.res"
{
	"InGameMainMenu" // [Ryker] Well apparently if you don't have this defined, the game just yeets your pause menu. It's still fine if you don't enable this or have it visible LOL
	{
		"ControlName"			"Frame"
		"fieldName"				"InGameMainMenu"
		"xpos"					"0"
		"ypos"					"0"
		"wide"					"f0"
		"tall"					"f0"
		"autoResize"			"0"
		"pinCorner"				"0"
		"visible"				"0"
		"enabled"				"0"
		"tabPosition"			"0"
		"PaintBackgroundType"	"0"
	}

	"PnlBackground"
	{
		"ControlName"			"L4DMenuBackground"
		"fieldName"				"PnlBackground"
		"xpos"					"0"
		"ypos"					"310"
		"zpos"					"-1"
		"wide"					"f0"
		"tall"					"135"
		"visible"				"1"
		"enabled"				"1"
		"fillColor"				"0 0 0 0"
	}

	"BtnGoIdle"
	{
		"ControlName"			"L4D360HybridButton"
		"fieldName"				"BtnGoIdle"
		"xpos"					"100"
		"ypos"					"335"
		"wide"					"180"
		"tall"					"20"
		"autoResize"			"1"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"1"
		"navUp"					"BtnExitToMainMenu"
		"navDown"				"BtnCallAVote"
		"labelText"				"#L4D360UI_InGameMainMenu_GoIdle"
		"style"					"MainMenuButton"
		"Command"				"GoIdle"
		"ActivationType"		"1"
	}

	"BtnCallAVote"
	{
		"ControlName"			"L4D360HybridButton"
		"fieldName"				"BtnCallAVote"
		"xpos"					"100"
		"ypos"					"355"
		"wide"					"180"
		"tall"					"20"
		"autoResize"			"1"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"0"
		"navUp"					"BtnGoIdle"
		"navDown"				"BtnOptions"
		"labelText"				"#L4D360UI_InGameMainMenu_CallAVote"
		"style"					"MainMenuButton"
		"command"				"FlmVoteFlyout"
		"ActivationType"		"1"
	}

	"BtnOptions"
	{
		"ControlName"			"L4D360HybridButton"
		"fieldName"				"BtnOptions"
		"xpos"					"100"
		"ypos"					"375"
		"wide"					"180"
		"tall"					"20"
		"autoResize"			"1"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"0"
		"navUp"					"BtnCallAVote"
		"navDown"				"BtnExitToMainMenu"
		"labelText"				"#L4D360UI_MainMenu_Options"
		"style"					"MainMenuButton"
		"command"				"FlmOptionsFlyout"
		"ActivationType"		"1"
	}

	"BtnExitToMainMenu"
	{
		"ControlName"			"L4D360HybridButton"
		"fieldName"				"BtnExitToMainMenu"
		"xpos"					"100"
		"ypos"					"405"
		"wide"					"180"
		"tall"					"20"
		"autoResize"			"1"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"0"
		"navUp"					"BtnOptions"
		"navDown"				"BtnGoIdle"
		"labelText"				"#L4D360UI_InGameMainMenu_ExitToMainMenu"
		"style"					"MainMenuButton"
		"command"				"ExitToMainMenu"
		"ActivationType"		"1"
	}

	"FlmOptionsFlyout"
	{
		"ControlName"			"FlyoutMenu"
		"fieldName"				"FlmOptionsFlyout"
		"visible"				"0"
		"wide"					"0"
		"tall"					"0"
		"zpos"					"3"
		"InitialFocus"			"BtnVideo"
		"ResourceFile"			"resource/UI/L4D360UI/OptionsFlyout.res"
	}

	"FlmVoteFlyout"
	{
		"ControlName"			"FlyoutMenu"
		"fieldName"				"FlmVoteFlyout"
		"visible"				"0"
		"wide"					"0"
		"tall"					"0"
		"zpos"					"3"
		"InitialFocus"			"BtnBootPlayer" // [Ryker] I rarely call votes and when I do, it's to kick people in pubs
		"ResourceFile"			"resource/UI/L4D360UI/InGameVoteFlyout.res"
	}

	"FlmVoteFlyoutVersus"
	{
		"ControlName"			"FlyoutMenu"
		"fieldName"				"FlmVoteFlyoutVersus"
		"visible"				"0"
		"wide"					"0"
		"tall"					"0"
		"zpos"					"3"
		"InitialFocus"			"BtnBootPlayer"
		"ResourceFile"			"resource/UI/L4D360UI/InGameVoteFlyoutVersus.res"
	}

	"FlmVoteFlyoutSurvival"
	{
		"ControlName"			"FlyoutMenu"
		"fieldName"				"FlmVoteFlyoutSurvival"
		"visible"				"0"
		"wide"					"0"
		"tall"					"0"
		"zpos"					"3"
		"InitialFocus"			"BtnBootPlayer"
		"ResourceFile"			"resource/UI/L4D360UI/InGameVoteFlyoutSurvival.res"
	}

	"FlmVoteFlyoutVersusSurvival"
	{
		"ControlName"			"FlyoutMenu"
		"fieldName"				"FlmVoteFlyoutVersusSurvival"
		"visible"				"0"
		"wide"					"0"
		"tall"					"0"
		"zpos"					"3"
		"InitialFocus"			"BtnBootPlayer"
		"ResourceFile"			"resource/UI/L4D360UI/InGameVoteFlyoutVersusSurvival.res"
	}

	"FlmVoteFlyoutSurvivalSolo"
	{
		"ControlName"			"FlyoutMenu"
		"fieldName"				"FlmVoteFlyoutSurvivalSolo"
		"visible"				"0"
		"wide"					"0"
		"tall"					"0"
		"zpos"					"3"
		"InitialFocus"			"BtnRestartScenario"
		"ResourceFile"			"resource/UI/L4D360UI/InGameVoteFlyoutSurvivalSolo.res"
	}
}